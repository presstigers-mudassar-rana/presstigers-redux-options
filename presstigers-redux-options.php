<?php

/*
  Plugin Name: Presstigers Redux Options
  Plugin URI: http://presstigers.com
  Description: Presstigers Redux Options | Overriding of Redux Theme options
  Version: 1.0
  Author: Mudasir Rana - PressTigers
  Author URI: http://presstiger.com
  Text Domain: Presstigers
 */

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pt-core-activator.php
 */
function activate_pt_core() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-pt-core-activator.php';
    Pt_Core_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pt-core-deactivator.php
 */
function deactivate_pt_core() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-pt-core-deactivator.php';
    Pt_Core_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_pt_core');
register_deactivation_hook(__FILE__, 'deactivate_pt_core');

/**
 * The core plugin class that is used to create Theme Otions,
 */
require plugin_dir_path(__FILE__) . 'includes/class-pt-theme-options.php';

/**
 * Load TGMA activator file.
 */
require plugin_dir_path(__FILE__) . '/includes/tgma/activator.php';
