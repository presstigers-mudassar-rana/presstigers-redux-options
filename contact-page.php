<?php
/**
 * Template Name: Contact us Page
 */

get_header(); 

?>
<div id="tabs">
    <ul>
        <?php
$sidebar_id = 'sidebar-2';
$sidebars_widgets = wp_get_sidebars_widgets();
$widget_ids = $sidebars_widgets[$sidebar_id]; 
    foreach( $widget_ids as $id ) {
        $wdgtvar = 'widget_'._get_widget_id_base( $id );
        $idvar = _get_widget_id_base( $id );
        $instance = get_option( $wdgtvar );
        $idbs = str_replace( $idvar.'-', '', $id );
?>

        <li>
            <a href=<?php echo '#'.$idbs;?>><?php echo $instance[$idbs]['title'];?></a>
        </li>
    
    <?php } ?>
</ul>
<?php
    foreach( $widget_ids as $id ) { 
     $sidebar_id = 'sidebar-2';
$sidebars_widgets = wp_get_sidebars_widgets();
$widget_ids = $sidebars_widgets[$sidebar_id]; 
    
        $wdgtvar = 'widget_'._get_widget_id_base( $id );
        $idvar = _get_widget_id_base( $id );
        $instance = get_option( $wdgtvar );
        $idbs = str_replace( $idvar.'-', '', $id );   
        ?>
    <div id="<?php echo '#'.$idbs;?>">
        <?php echo $instance[$idbs]['text'];?>
    </div>
    <?php }?>            
</div>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main> 
	</div> 

<?php
//get_sidebar();
get_footer();


/**
 * Function that will show the Metebox Image in the Size Tab of WooCommerce Detail Page
 */
function tw_wc_pdf_product_tab_content() {

    global $product;
    $post_id = $product->id;
    $image_url = get_post_meta($post_id , 'tw_size_image' , true);
    
    $html = '<iframe src="http://klein.dev/product/happy-ninja/?wpp_export=pdf"></iframe>';
    echo $html;
}
/**
 * Hook in and add a new Product Tab on WooCommerce Detail Page
 */
add_filter( 'woocommerce_product_tabs', 'tw_new_product_tab' );
function tw_new_product_tab( $tabs ) {
	
	// Adds the new tab
//	$tabs['size_tab'] = array(
//		'title' 	=> __( 'Size', 'woocommerce' ),
//		'priority' 	=> 50,
//		'callback' 	=> 'tw_wc_new_product_tab_content'
//	);
        $tabs['pdf_tab'] = array(
		'title' 	=> __( 'PDF', 'woocommerce' ),
		'priority' 	=> 60,
		'callback' 	=> 'tw_wc_pdf_product_tab_content'
	);

	return $tabs;

}

add_filter('woocommerce_get_price', 'custom_price_WPA111772', 10, 2);

/**
 * custom_price_WPA111772 
 *
 * filter the price based on category and user role
 * @param  $price   
 * @param  $product 
 * @return 
 */
function custom_price_WPA111772($price, $product) {

    $orginal_price = $price;
    
    // Currency that will be use for conversion
    $default_currency = get_option( 'wc_pricing_default_currency_start', 1 );

    $currency_code = get_woocommerce_currency();
    if ($currency_code == $default_currency) {
        return $orginal_price;
    }
    else{
        $amount = urlencode($price);
        $from_Currency = urlencode($default_currency);
        $to_Currency = urlencode($currency_code);
        $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);  
        $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
        $price = $converted_amount;
        return $price;
    }
}

add_filter('woocommerce_general_settings', 'add_default_currency_option_setting');
function add_default_currency_option_setting($settings) {

    $updated_settings = array();
    
    $currency_code_options = get_woocommerce_currencies();
    foreach ($currency_code_options as $code => $name) {
        $currency_code_options[$code] = $name . ' (' . get_woocommerce_currency_symbol($code) . ')';
    }

    foreach ($settings as $section) {

        // at the bottom of the Pricing Options section
        if (isset($section['id']) && 'pricing_options' == $section['id'] &&
                isset($section['type']) && 'sectionend' == $section['type']) {

            $updated_settings[] = array(
                'name' => __('Choose Your Default Currency', 'wc_pricing_default_currency'),
                'desc_tip' => __('Choose your currency which will be use as a conversion. If both currencies are same, the price will remain same', 'wc_pricing_default_currency'),
                'id' => 'wc_pricing_default_currency_start',
                'type' => 'select',
                'class'    => 'wc-enhanced-select',
                'std' => '1', // WC < 2.0
                'default' => '1', // WC >= 2.0
                'options' => $currency_code_options
            );
        }
        $updated_settings[] = $section;
    }
    return $updated_settings;
}
