<?php

/**
 * For full documentation, please visit: http://docs.reduxframework.com/
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 */
if (!class_exists('Redux')) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "pt_theme_options";

global $pt_theme_options;
$pt_theme_options = get_option('pt_theme_options');

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters('redux_demo/opt_name', $opt_name);

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns = array();

if (is_dir($sample_patterns_path)) {

    if ($sample_patterns_dir = opendir($sample_patterns_path)) {
        $sample_patterns = array();

        while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

            if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                $name = explode('.', $sample_patterns_file);
                $name = str_replace('.' . end($name), '', $sample_patterns_file);
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => __('Presstigers Options', 'redux-framework-demo'),
    'page_title' => __('Presstigers Options', 'redux-framework-demo'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => 'dashicons-admin-customizer',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.
    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'red',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);

// Add content after the form.
$args['footer_text'] = __('<p>Copyrights © Presstigers 2016</p>', 'redux-framework-demo');

Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id' => 'redux-help-tab-1',
        'title' => __('Theme Information 1', 'redux-framework-demo'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
    ),
    array(
        'id' => 'redux-help-tab-2',
        'title' => __('Theme Information 2', 'redux-framework-demo'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
    )
);
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = __('<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo');
Redux::setHelpSidebar($opt_name, $content);


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*

  As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


 */



/* -------------Typography------------- */
Redux::setSection($opt_name, array(
    'title' => __('Typography', 'redux-framework-demo'),
    'id' => 'typography',
    'desc' => __('', 'redux-framework-demo'),
    'icon' => 'el el-font',
    'fields' => array(
        array(
            'id' => 'opt-typography-body',
            'type' => 'typography',
            'title' => __('Body Font', 'redux-framework-demo'),
            'subtitle' => __('Specify the body font properties.', 'redux-framework-demo'),
            'google' => true,
            'output' => array('body'),
            'default' => array(
                'color' => '#dd9933',
                'font-size' => '30px',
                'font-family' => 'Arial,Helvetica,sans-serif',
                'font-weight' => 'Normal',
            ),
        ),
        array(
            'id' => 'opt-typography-h1',
            'type' => 'typography',
            'title' => __('H1 Font', 'redux-framework-demo'),
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('h1'),
            'units' => 'px',
            // Defaults to px
            'subtitle' => __('Select the Typography for H1 Tag', 'redux-framework-demo'),
            'default' => array(
                'color' => '#333',
                'font-style' => '700',
                'font-family' => 'Abel',
                'google' => true,
                'font-size' => '48px',
                'line-height' => '1.05em'
            ),
        ),
        array(
            'id' => 'opt-typography-h2',
            'type' => 'typography',
            'title' => __('H2 Font', 'redux-framework-demo'),
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('h2'),
            'units' => 'px',
            // Defaults to px
            'subtitle' => __('Select the Typography for H2 Tag', 'redux-framework-demo'),
            'default' => array(
                'color' => '#333',
                'font-style' => '700',
                'font-family' => 'Abel',
                'google' => true,
                'font-size' => '36px',
                'line-height' => '1.25em'
            ),
        ),
        array(
            'id' => 'opt-typography-h3',
            'type' => 'typography',
            'title' => __('H3 Font', 'redux-framework-demo'),
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('h3'),
            'units' => 'px',
            // Defaults to px
            'subtitle' => __('Select the Typography for H3 Tag', 'redux-framework-demo'),
            'default' => array(
                'color' => '#333',
                'font-style' => '700',
                'font-family' => 'Abel',
                'google' => true,
                'font-size' => '28px',
                'line-height' => '1.25em'
            ),
        ),
        array(
            'id' => 'opt-typography-h4',
            'type' => 'typography',
            'title' => __('H4 Font', 'redux-framework-demo'),
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('h4'),
            'units' => 'px',
            // Defaults to px
            'subtitle' => __('Select the Typography for H4 Tag', 'redux-framework-demo'),
            'default' => array(
                'color' => '#333',
                'font-style' => '700',
                'font-family' => 'Abel',
                'google' => true,
                'font-size' => '18px',
                'line-height' => '1.22222222em'
            ),
        ),
        array(
            'id' => 'opt-typography-paragraph',
            'type' => 'typography',
            'title' => __('Paragraph Font', 'redux-framework-demo'),
            'subtitle' => __('Specify the body font properties.', 'redux-framework-demo'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('p'),
            'default' => array(
                'color' => '#dd9933',
                'font-size' => '16px',
                'font-family' => 'sans-serif',
                'font-weight' => 'Normal',
                'line-height' => '1em'
            ),
        ),
    )
));


/* -------------Header Section------------- */

Redux::setSection($opt_name, array(
    'title' => __('Header', 'redux-framework-demo'),
    'id' => 'header',
    'desc' => __('', 'redux-framework-demo'),
    'icon' => 'el el-picture'
));

Redux::setSection($opt_name, array(
    'title' => __('Upload Logo', 'redux-framework-demo'),
    'id' => 'header-logo',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'header-title',
            'type' => 'text',
            'title' => __('Header Title', 'redux-framework-demo'),
            'subtitle' => __('Write a title for your header', 'redux-framework-demo'),
            'desc' => __('Optional', 'redux-framework-demo'),
            'placeholder' => 'Write Title',
        ),
        array(
            'id' => 'header-logo',
            'type' => 'media',
            'url' => true,
            'title' => __('choose header logo', 'redux-framework-demo'),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc' => __('Basic image uploader with disabled URL input field.', 'redux-framework-demo'),
            'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
            'default' => array('url' => plugin_dir_url(__FILE__) . 'images/logo.png'),
        )
    )
));

/* ------------- Slider Section------------- */
Redux::setSection($opt_name, array(
    'title' => __('Slider', 'redux-framework-demo'),
    'id' => 'slider-section',
    'desc' => __('', 'redux-framework-demo'),
    'icon' => 'el el-picture'
));

Redux::setSection($opt_name, array(
    'title' => __('Add your Slides', 'redux-framework-demo'),
    'id' => 'slides-option-section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'upload-slide',
            'type' => 'slides',
            'title' => __('Slider Title', 'redux-framework-demo'),
            'subtitle' => __('Write a description for your slider', 'redux-framework-demo'),
        )
    )
));

$slider_shortcode_title = $pt_theme_options['upload-slide'][0]['title'];
$slider_shortcode_image = $pt_theme_options['upload-slide'][0]['image'];

$slider_content = "<b>". '[pt_slider title = "'.$slider_shortcode_title.'" image_url = "'.$slider_shortcode_image.'" ]' . "</b>";

Redux::setSection($opt_name, array(
    'title' => __('Slider Shortcode', 'redux-framework-demo'),
    'id' => 'slider-shortocode-section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'slider-shortcode-section',
            'type' => 'raw',
            'title' => __('', 'redux-framework-demo'),
            'subtitle' => __('Copy the shortcode and paste it to you post/page to display Slider', 'redux-framework-demo'),
            'content' => $slider_content
        )
    )
));

/* ------------- Sections End ------------- */

/* -------------Footer Section------------- */
Redux::setSection($opt_name, array(
    'title' => __('Footer', 'redux-framework-demo'),
    'id' => 'footer-section',
    'desc' => __('', 'redux-framework-demo'),
    'icon' => 'el el-plus'
));

Redux::setSection($opt_name, array(
    'title' => __('Copyright Description', 'redux-framework-demo'),
    'id' => 'footer-copyright',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'footer-copyright-text',
            'type' => 'textarea',
            'title' => __('Footer Detail', 'redux-framework-demo'),
            'subtitle' => __('Write a description for your footer', 'redux-framework-demo'),
            'desc' => __('Optional', 'redux-framework-demo'),
        )
    )
));

/* ------------- Sections End ------------- */
    




