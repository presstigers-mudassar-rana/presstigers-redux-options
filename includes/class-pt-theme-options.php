<?php

/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc.
 */
if (!defined('WPINC')) {
    die;
}

class Pt_Theme_Options {

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @since   1.0.0
     *
     * @var	 string
     */
    const VERSION = '1.0.0';

    /**
     * Instance of this class.
     *
     * @since	1.0.0
     *
     * @var	  object
     */
    protected static $instance = null;

    /**
     * Construct the plugin object
     * 
     * @since 1.0.0
     */
    private function __construct() {

        add_action('plugins_loaded', array(&$this, 'pt_theme_options_load_textdomain'));
        add_action('redux/construct', array(&$this, 'pt_disable_dev_mode_plugin'));
        add_action('init', array(&$this, 'pt_remove_demo_mode_link'));
        require_once(dirname(__FILE__) . '/config/pt-config.php');
    }

    /**
     * Return an instance of this class.
     *
     * @since	 1.0.0
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Load the plugin Text Domain
     *
     * @since 1.0.0
     */
    public function pt_theme_options_load_textdomain() {
        load_plugin_textdomain('presstigers-redux-options', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

    /**
     * Function removes the Notices & Demo ULRs from the redux-framework plugin
     * 
     * @since 1.0.0
     */
    public function pt_remove_demo_mode_link() {

        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
                    ), null, 2);

            remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
        }
    }

    /**
     * Disabling Mode of Redux Development
     * 
     * @since 1.0.0
     */
    public function pt_disable_dev_mode_plugin($redux) {
        if ($redux->args['opt_name'] != 'redux_demo') {
            $redux->args['dev_mode'] = false;
            $redux->args['forced_dev_mode_off'] = false;
        }
    }

    /**
     * Function remove Redex Documentation page from Dashboard Menu ( Tools -> Redux Framework ) 
     * 
     * @since 1.0.0
     */
    public function pt_remove_submenu_under_tools() {
        remove_submenu_page('tools.php', 'redux-about');
    }

}

// END Class Pt_Theme_Options


if (class_exists('Pt_Theme_Options')) {
    add_action('plugins_loaded', array('Pt_Theme_Options', 'get_instance'));
}
