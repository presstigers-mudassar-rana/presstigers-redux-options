<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://presstigers.com
 * @since      1.0.0
 *
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @author     Mudasir Rana <mudassar.rana@nxb.com.pk>
 */
class Pt_Core_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
