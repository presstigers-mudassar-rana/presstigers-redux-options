<?php

/**
 * Fired during plugin activation
 *
 * @link       http://presstigers.com
 * @since      1.0.0
 *
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @author     Mudasir Rana <mudassar.rana@nxb.com.pk>
 */
class Pt_Core_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
